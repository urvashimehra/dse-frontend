import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {
  private dataSource = new BehaviorSubject({});
  currentSelectFeature = this.dataSource.asObservable();
//for affecting feature
  private dataSourcAaffectingFeature = new BehaviorSubject({});
  currentSelectAaffectingFeature = this.dataSourcAaffectingFeature.asObservable();
//for Performance feature
private dataSourcPerformanceFeature = new BehaviorSubject({});
currentSelectPerformanceFeature = this.dataSourcPerformanceFeature.asObservable();

//for Bar Chart
private dataSourcBarChart = new BehaviorSubject({});
currentSelectBarChart = this.dataSourcBarChart.asObservable();

  constructor() { }

  changeSelectFeature(selectedFeatureData:object) {
    this.dataSource.next(selectedFeatureData)
  }

  changeSelectAaffectingFeature(selectedFeatureData:object) {
    this.dataSourcAaffectingFeature.next(selectedFeatureData)
  }
  
  changeSelectPerformanceFeature(selectedFeatureData:object) {
    this.dataSourcPerformanceFeature.next(selectedFeatureData)
  }

  changeSelectBarChart(selectedFeatureData:object) {
    this.dataSourcBarChart.next(selectedFeatureData)
  }
}