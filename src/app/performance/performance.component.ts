import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import {SharedServiceService} from '../shared-service.service';

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.scss']
})
export class PerformanceComponent implements OnInit {
  graphData:object;    
  data = [];

highcharts = Highcharts;
updateFlag:boolean=false;
chartOptions = {   
chart: {
type: "spline"
},
title: {
text: ""
},
xAxis:{
categories:["2013", "2014", "2015", "2016", "2017", "2018"]
},
yAxis: {          
title:{
    text:""
} 
},
legend: {
  enabled: false
},
series: [{name: '',data: []}]
};
  constructor(private dataPerformanceFeature: SharedServiceService) { }

  ngOnInit() {
    this.dataPerformanceFeature.currentSelectAaffectingFeature.subscribe(selectedFeatureData => {
      this.graphData = selectedFeatureData;
      this.chartOptions.xAxis.categories=Object.keys(this.graphData);
    this.chartOptions.series[0].data = Object.values(this.graphData);
    this.updateFlag =true;
    });
  }

}
