import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectingFeaturesComponent } from './affecting-features.component';

describe('AffectingFeaturesComponent', () => {
  let component: AffectingFeaturesComponent;
  let fixture: ComponentFixture<AffectingFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectingFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectingFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
