import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import {SharedServiceService} from '../shared-service.service';

@Component({
  selector: 'app-affecting-features',
  templateUrl: './affecting-features.component.html',
  styleUrls: ['./affecting-features.component.scss']
})
export class AffectingFeaturesComponent implements OnInit {
  graphData:object;  
  highcharts = Highcharts;
  updateFlag:boolean=false;
  chartOptions = {   
    chart: {
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
    text: ""
    },
    plotOptions : {
      pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      shadow: false,
      center: ['50%', '50%'],
      size:'60%',
      innerSize: '20%'            
      }
    },
    legend: {
      enabled: false
    },
    series : [{
      type: 'pie',
      name: 'Affecting Features',
      data:  []
    }]
  };
  constructor(private dataAffectingFeature: SharedServiceService) { }

  ngOnInit() {
    this.dataAffectingFeature.currentSelectAaffectingFeature.subscribe(selectedFeatureData => {
      this.graphData = selectedFeatureData;
      let data = [];
      for (let entry of Object.entries(this.graphData)) {
        data.push({'name':entry[0],'y':entry[1]});
      }
      //this.chartOptions.xAxis.categories=Object.keys(this.graphData);
      this.chartOptions.series[0].data = data;
      this.updateFlag =true;

   // console.log(Object.keys(this.graphData));
    });
  }

}
