import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import {SharedServiceService} from '../shared-service.service';


@Component({
  selector: 'app-dependent-features',
  templateUrl: './dependent-features.component.html',
  styleUrls: ['./dependent-features.component.scss']
})
export class DependentFeaturesComponent implements OnInit {
 // @Input() public formData:object; 
 formData:object; 
 //loadChart(data): any {}
  highcharts = Highcharts;
  updateFlag:boolean=false;
  chartOptions = {   
  chart: {
  type: "bar"
  },
  title: {
  text: ""
  },
  xAxis:{
  categories:[]
  },
  yAxis: {          
  title:{
      text:""
  } 
  },
  legend: {
    enabled: false
  },
  series: [{
      name: '',
      data: []
    }]
  };

  constructor(private dataDependentFeature: SharedServiceService) { }

  ngOnInit() {
   
    this.dataDependentFeature.currentSelectFeature.subscribe(selectedFeatureData => {
      this.formData = selectedFeatureData;
      this.chartOptions.xAxis.categories=Object.keys(this.formData);
    this.chartOptions.series[0].data = Object.values(this.formData);
    this.updateFlag =true;
    console.log(Object.keys(this.formData));
    });
   // this.chartOptions.xAxis.categories=Object.keys(this.formData);
   // this.chartOptions.series[0].data = Object.values(this.formData);
   // console.log(Object.keys(this.formData));
    //console.log(Object.values(this.formData));
   // console.log(this.chartOptions);
  }

}
