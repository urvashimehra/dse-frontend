import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependentFeaturesComponent } from './dependent-features.component';

describe('DependentFeaturesComponent', () => {
  let component: DependentFeaturesComponent;
  let fixture: ComponentFixture<DependentFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependentFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependentFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
