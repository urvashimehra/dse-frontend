import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import * as Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);

import {SharedServiceService} from '../shared-service.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  @ViewChild('agGrid', {static: true}) agGrid: AgGridAngular;
  recdData:object;
  gridData:object;
  algoData:object;

  columnDefs = [
    {headerName: 'alpha value', field: 'alpha value', sortable: true },
    {headerName: 'slope', field: 'slope', sortable: true },
    {headerName: 'seed', field: 'seed', sortable: true},
    {headerName: 'initial node weight', field: 'initial node weight', sortable: true},
    {headerName: 'hidden layers', field: 'hidden layers', sortable: true},
    {headerName: 'nodes per layer', field: 'nodes per layer', sortable: true}
  ];
  rowData: any;

  highcharts = Highcharts;
  updateFlag:boolean=false;
  chartOptions = {   
  chart: {
  type: "column",
    events: {
      drilldown: function(e) {      
          this.setTitle({ text: ('Dataset Behaviour for ' + e.point.name) }, {text: ''});
      },
      drillup: function(e) {
        if(e.seriesOptions.name == 'Dataset Behaviour') {
          this.setTitle({text: 'Dataset Behaviour'});
        }
        else {
          this.setTitle({text: 'Dataset Behaviour for ' + e.seriesOptions.name});
        }
      }
    }
  },
  title: {
  text: 'Dataset Behaviour'
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
        text: 'Average Value'
    }
},
  legend: {
    enabled: false
  },
  series: [{
    name: 'Dataset Behaviour',
    colorByPoint: true,
    data: []
}],
drilldown: {
  series: []
}
  };
  constructor(private dataBarGraph: SharedServiceService) { }

  ngOnInit() {
    this.dataBarGraph.currentSelectBarChart.subscribe(selectedFeatureData => {
      this.recdData = selectedFeatureData;
      let tmpData={};
      let tmpGrData=[];
      let tmpGrdData=[];
      for (let [key, value] of Object.entries(this.recdData)) {
        
        if(key == "hyperparameter_grid"){
          tmpGrdData =  [value];
        } else if(typeof value == "object"){
          //break;
          tmpGrData.push({'name':key,'y':Object.keys(value).length,'drilldown':key});   
          this.createDrillDownData(key, value);
        } else{
          tmpData[key] =  [value];
        }       
      }
      this.algoData = tmpData;
      this.gridData = tmpGrdData;
      this.chartOptions.series[0].data = Object.values(tmpGrData);
      this.updateFlag =true;
      this.rowData = tmpGrdData;
      
    });
  }
  createDrillDownData(key:string, value:any){
    let val:any =value;
    let dt:any=[];
    for (let [key, value] of Object.entries(val)) {
      if(typeof value == "object"){           
          dt.push({'name':key,'y':Object.keys(value).length,'drilldown':key});
          this.createDrillDownData(key, value);   
      } else{
        dt.push([key, value]); 
      }
    }
    this.chartOptions.drilldown.series.push({'id':key,'name':key,'data':dt});     
  }
  
}
