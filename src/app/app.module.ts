import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HighchartsChartComponent } from 'highcharts-angular';

import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms'; 
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { ResultComponent } from './result/result.component';
import { AffectingFeaturesComponent } from './affecting-features/affecting-features.component';
import { DependentFeaturesComponent } from './dependent-features/dependent-features.component';
import { PerformanceComponent } from './performance/performance.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    HighchartsChartComponent,
    HomeComponent,
    ResultComponent,
    AffectingFeaturesComponent,
    DependentFeaturesComponent,
    PerformanceComponent,
    BarChartComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgGridModule.withComponents([])
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
